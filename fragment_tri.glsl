#version 330 core
in vec3 ourColor;
out vec4 FragColour;

void main()
{
    FragColour = vec4(ourColor, 1.0);
    //FragColour = vec4(1.0, 0.6, 0.6, 1.0);
}